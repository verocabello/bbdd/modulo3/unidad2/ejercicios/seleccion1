<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrud() {
        return $this->render("gestion");
    }
    
    
    public function actionConsulta1a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta1() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta2a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
    public function actionConsulta2() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach"')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo="Artiach"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
    public function actionConsulta3a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("edad")
                ->distinct()
                ->where("nomequipo='Artiach' or nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"G_avg(edad)  (σ_(nomequipo='Banesto')(ciclista)",
        ]);
    }
    
    public function actionConsulta3() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach" or nomequipo="Amore Vita"')
                ->queryScalar();
        
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo="Artiach" or nomequipo="Amore Vita"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=> "Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach' or nomequipo='Amore Vita'",
        ]);
    }
    
    public function actionConsulta4a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("dorsal")
                ->distinct()
                ->where("edad<25 or edad>30"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad<25 or edad>30",
        ]);
    }
    
    public function actionConsulta4() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(*) from ciclista where edad<25 or edad>30')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad<25 or edad>30',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"select dorsal from ciclista where edad<25 or edad>30",
        ]);
    }
    
    public function actionConsulta5a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("dorsal")
                ->distinct()
                ->where("edad between 28 and 32 and nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto",
            "sql"=>"select dorsal from ciclista where edad between 28 and 32 and nomequipo='Banesto'",
        ]);
    }    
    
    public function actionConsulta5() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(*) from ciclista where edad between 28 and 32 and nomequipo="Banesto"')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad between 28 and 32 and nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto",
            "sql"=>"select dorsal from ciclista where edad between 28 y 32 and nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta6a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select("nombre")
                ->distinct()
                ->where("length(nombre)>8"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=> "Consulta 6 con Active Record",
            "enunciado"=>"Indica el nombre de los ciclistas que el número de caracteres sea mayor que 8",
            "sql"=>"select nombre from ciclista where length(nombre)>8",
        ]);
    }    
    
    public function actionConsulta6() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(*) from ciclista where length(nombre)>8')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'select nombre from ciclista where length(nombre)>8',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=> "Consulta 6 con DAO",
            "enunciado"=>"Indica el nombre de los ciclistas que el número de caracteres sea mayor que 8",
            "sql"=>"select nombre from ciclista where length(nombre)>8",
        ]);
    }
    
    public function actionConsulta7a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()
                ->select(["nombre","dorsal","upper(nombre) Nmayus"])
                ->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','Nmayus'],
            "titulo"=> "Consulta 7 con Active Record",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"select nombre,dorsal,upper(nombre) from ciclista",
        ]);
    }    
    
    public function actionConsulta7() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(*) from ciclista')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'select nombre,dorsal,upper(nombre) Nmayus from ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','Nmayus'],
            "titulo"=> "Consulta 7 con DAO",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"select nombre,dorsal,upper(nombre) from ciclista",
        ]);
    }
    
    public function actionConsulta8a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Lleva::find()
                ->select("dorsal")
                ->distinct()
                ->where("código='MGE'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han llevado el maillot MGE(amarillo) en alguna etapa",
            "sql"=>"select distinct dorsal from lleva where código='MGE'",
        ]);
    }    
    
    public function actionConsulta8() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('select count(distinct dorsal) from lleva where código="MGE"')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'select distinct dorsal from lleva where código="MGE"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han llevado el maillot MGE(amarillo) en alguna etapa",
            "sql"=>"select distinct dorsal from lleva where código='MGE'",
        ]);
    }
    
    public function actionConsulta9a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("nompuerto")
                ->where("altura>1500"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=> "Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor que 1500",
            "sql"=>"select nompuerto from puerto where altura>1500",
        ]);
    }    
    
        public function actionConsulta9() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT count(nompuerto) FROM puerto WHERE altura>1500')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT nompuerto FROM puerto WHERE altura>1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=> "Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor que 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura>1500",
        ]);
    }
    
    public function actionConsulta10a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("dorsal")
                ->distinct()
                //->where("altura BETWEEN 1800 AND 3000 OR pendiente>8")
                ->where([">","pendiente",8])
                ->orWhere(["between","altura",1800,3000]),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente>8",
        ]);
    }    
    
    public function actionConsulta10() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT count(DISTINCT dorsal) FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente>8')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente>8',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 OR pendiente>8",
        ]);
    }
    
    public function actionConsulta11a() {
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()
                ->select("dorsal")
                ->distinct()
                //->where("altura BETWEEN 1800 AND 3000 AND pendiente>8")
                ->where([">","pendiente",8])
                ->andWhere(["between","altura",1800,3000]),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente>8",
        ]);
    }    
    
    public function actionConsulta11() {
        // mediante DAO
        $numero=Yii::$app
                ->db
                ->createCommand('SELECT count(DISTINCT dorsal) FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente>8')
                ->queryScalar();
                
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente>8',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]); 

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=> "Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000 AND pendiente>8",
        ]);
    }
}